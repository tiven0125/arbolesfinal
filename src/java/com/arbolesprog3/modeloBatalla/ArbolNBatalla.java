/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arbolesprog3.modeloBatalla;

import com.arbolesprog3.excepcion.BarcoExcepcion;
import com.sun.javafx.scene.control.skin.VirtualFlow;
import java.io.Serializable;
import java.util.ArrayList;
import static java.util.Collections.list;
import java.util.List;

/**
 *
 * @author duvanandres
 */
public class ArbolNBatalla implements Serializable {

    NodoN raiz;
    int cantidadNodos;
    List<Coordenada> coordendas;

    public ArbolNBatalla() {
        coordendas = new ArrayList<>();
    }

    public NodoN getRaiz() {
        return raiz;
    }

    public void setRaiz(NodoN raiz) {
        this.raiz = raiz;
    }

    public int getCantidadNodos() {
        return cantidadNodos;
    }

    public void setCantidadNodos(int cantidadNodos) {
        this.cantidadNodos = cantidadNodos;
    }

    public List<Coordenada> getCoordendas() {
        return coordendas;
    }

    public void setCoordendas(List<Coordenada> coordendas) {
        this.coordendas = coordendas;
    }

    public boolean esHoja(NodoN nodo) {
        if (nodo.equals(raiz)) {
            return false;

        }

        return nodo.getHijos() == null;

    }

    public NodoN buscarNodoxNombre(String nombre) {
        if (raiz != null) {
            return buscarNodoxNombre(nombre, raiz);
        }
        return null;
    }

    private NodoN buscarNodoxNombre(String nombre, NodoN pivote) {
        if (pivote.getDato().getTipobarco().getNombre().compareTo(nombre) == 0) {
            return pivote;
        } else {
            for (NodoN hijo : pivote.getHijos()) {
                NodoN padreEncontrado = buscarNodoxNombre(nombre, hijo);
                if (padreEncontrado != null) {
                    return padreEncontrado;
                }
            }
        }
        return null;
    }

    public BarcoPosicionado buscarBarcoxNombre(String nombre) {
        if (raiz != null) {
            return buscarBarcoxNombre(nombre, raiz);
        }
        return null;
    }

    private BarcoPosicionado buscarBarcoxNombre(String nombre, NodoN pivote) {
        if (pivote.getDato().getTipobarco().getNombre().compareTo(nombre) == 0) {
            return pivote.getDato();
        } else {
            for (NodoN hijo : pivote.getHijos()) {
                NodoN padreEncontrado = buscarNodoxNombre(nombre, hijo);
                if (padreEncontrado != null) {
                    return padreEncontrado.getDato();
                }
            }
        }
        return null;
    }

    public NodoN buscarPadre(String nombre) {
        if (raiz.getDato().getTipobarco().getNombre().compareTo(nombre) == 0) {
            return null;
        }
        return buscarPadre(nombre, raiz);
    }

    private NodoN buscarPadre(String nombre, NodoN pivote) {
        for (NodoN hijo : pivote.getHijos()) {
            if (hijo.getDato().getTipobarco().getNombre().compareTo(nombre) == 0) {
                return pivote;
            } else {
                NodoN padreBuscado = buscarPadre(nombre, hijo);
                if (padreBuscado != null) {
                    return padreBuscado;
                }
            }
        }
        return null;
    }

    public void adicionarNodo(BarcoPosicionado dato, BarcoPosicionado padre) {
        if (raiz == null) {
            raiz = new NodoN(dato);
        } else {
            NodoN padreEncontrado = buscarNodoxNombre(padre.getTipobarco().getNombre());
            if (padreEncontrado != null) {
                padreEncontrado.getHijos().add(new NodoN(dato));
            }

        }
        cantidadNodos++;
    }

    public void adicionarNodoxCodigo(BarcoPosicionado dato, BarcoPosicionado padre) {
        if (raiz == null) {
            raiz = new NodoN(dato);

        } else {
            adicionarNodoxCodigo(dato, padre, raiz);

        }
        cantidadNodos++;
    }

    public boolean adicionarNodoxCodigo(BarcoPosicionado dato, BarcoPosicionado padre, NodoN pivote) {
        // boolean adicionado=false;
        if (pivote.getDato().getIdentificador() == padre.getIdentificador()) {
            //Es el padre donde debo adicionar
            pivote.getHijos().add(new NodoN(dato));
            //adicionado=true;
            return true;
        } else {
            for (NodoN hijo : pivote.getHijos()) {
                if (adicionarNodoxCodigo(dato, padre, hijo)) {
                    break;
                }

            }
        }
        return false;
    }

    public void reiniciarBarcos() {
        for (BarcoPosicionado listarBarcosPosicionado : listarBarcosPosicionados()) {
            listarBarcosPosicionado.setCoordenadas(null);
        }
        for (BarcoPosicionado listarBarcos : listarBarcos()) {
            listarBarcos.setCoordenadas(null);
        }
        for (BarcoPosicionado listarBarcosDestruidos : listarBarcosDestruidos()) {
            listarBarcosDestruidos.setCoordenadas(null);
        }
    }

    public List<BarcoPosicionado> listarBarcosPosicionados() {
        List<BarcoPosicionado> listaBarcos = new ArrayList<>();

        listarBarcosPosicionados(raiz, listaBarcos);
        return listaBarcos;
    }

    private void listarBarcosPosicionados(NodoN nodo, List<BarcoPosicionado> barcos) {

        for (NodoN agregar : nodo.getHijos()) {

            barcos.add(agregar.getDato());

            listarBarcosPosicionados(agregar, barcos);

        }
    }

    public List<BarcoPosicionado> listarBarcos() {
        List<BarcoPosicionado> listaBarcos = new ArrayList<>();

        listarBarcos(raiz, listaBarcos);
        return listaBarcos;
    }

    private void listarBarcos(NodoN nodo, List<BarcoPosicionado> barcos) {

        for (NodoN agregar : nodo.getHijos()) {
            if (agregar.getDato().getCoordenadas().isEmpty()) {
                barcos.add(agregar.getDato());
            }

            listarBarcos(agregar, barcos);

        }
    }

    public List<BarcoPosicionado> listarBarcosDestruidos() {
        List<BarcoPosicionado> listaBarcosDestruidos = new ArrayList<>();

        listarBarcosDestruidos(raiz, listaBarcosDestruidos);
        return listaBarcosDestruidos;
    }

    private void listarBarcosDestruidos(NodoN nodo, List<BarcoPosicionado> barcos) {

        for (NodoN agregar : nodo.getHijos()) {
            if (agregar.getDato().getEstado().getCodigo() == 2) {
                barcos.add(agregar.getDato());
            }

            listarBarcosDestruidos(agregar, barcos);

        }
    }
//    
//    public Coordenada[] instanciarCoordenadas(Coordenada coordenada, String direccion){
//        return instanciarCoordenadas(coordenada, direccion, raiz.dato.getTipobarco());
//    }

    public void instanciarCoordenadas(BarcoPosicionado barco, String direccion, int fila, int columna) throws BarcoExcepcion {
        if (barco.getCoordenadas().isEmpty()) {
            barco.setCoordenadas(new ArrayList<>());
        }
        if (direccion.equals("Vertical")) {
            for (int i = 0; i < barco.getTipobarco().getNroCasillas(); i++) {
                coordendas.add(new Coordenada((byte) (columna), (byte) (fila + i)));

//                    throw new BarcoExcepcion("Coordenadas Ocupadas");
            }
            if (verificarCoordenadaN(coordendas) == false) {
                barco.getCoordenadas().addAll(coordendas);
                coordendas = new ArrayList<>();
            } else {
                coordendas = new ArrayList<>();
                throw new BarcoExcepcion("Coordenadas Ocupadas");
            }

        } else if (direccion.equals("Horizontal")) {
            for (int i = 0; i < barco.getTipobarco().getNroCasillas(); i++) {
                coordendas.add(new Coordenada((byte) (columna + i), (byte) (fila)));

            }
            if (verificarCoordenadaN(coordendas) == false) {
                barco.getCoordenadas().addAll(coordendas);
                coordendas = new ArrayList<>();
            } else {
                coordendas = new ArrayList<>();
                throw new BarcoExcepcion("Coordenadas Ocupadas");

            }
        }

    }

    public boolean verificarCoordenadaN(List<Coordenada> coordenada) {
        if (verificarCoordenadaN(raiz, coordenada) == false) {
            return false;
        } else {
            return true;
        }

    }

    private boolean verificarCoordenadaN(NodoN pivote, List<Coordenada> coordenada) {
        boolean seEncontro = false; //se encontro la coordenada

        if (tieneCoordenadas(pivote.getDato())) {

            for (int i = 0; i < pivote.getDato().getCoordenadas().size(); i++) {

                for (int j = 0; j < coordenada.size(); j++) {
                    if (pivote.getDato().getCoordenadas().get(i).getColumna() == coordenada.get(j).getColumna() && pivote.getDato().getCoordenadas().get(i).getFila() == coordenada.get(j).getFila()) {
                        seEncontro = true;

                    }
                }
            }
        }
        if (seEncontro == false) {
            for (NodoN hijo : pivote.getHijos()) {
                seEncontro = verificarCoordenadaN(hijo, coordenada);
                if (seEncontro) {
                    break;
                }
            }
        }

        return seEncontro;
    }

    public boolean tieneCoordenadas(BarcoPosicionado barcoPosicionado) {
        if (barcoPosicionado.getCoordenadas() != null) {
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return "( " + raiz + ')';
    }

}
